<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: liuyuanlinlin
  Date: 2018/1/26
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 使用springmvc的表单标签-->
<!--  作用：
    更快速的开发表单页面
    更方便的进行表单回显
注意：
可以通过 modelAttribute 属性指定绑定的模型属性,
若没有指定该属性，则默认从 request 域对象中读取 command 的表单 bean
如果该属性值也不存在，则会发生错误。（springmvc默认一定要回显的）
1、modelAttribute="employee"
2、map.put("employee",new Employee());
-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <form:form action="emp" method="post" modelAttribute="employee">

        <!-- 修改的时候， LastName是不能被修改的-->
        <!-- 这是添加-->
        <c:if test="${employee.id==null}">
            LastName LastName:<form:input path="lastName"/>
        </c:if>
        <!-- 这是修改-->
        <c:if test="${employee.id!=null}">
            <!-- form当用于表单回显的时候，要求form标签里的path所对应的属性与之modelAttribute的bean里面必须有对应的属性才可以
                所以这里使用了put

            _method不能使用form:hidden属性
            -->
            <form:hidden path="id"/>
            <input type="hidden" name="_method" value="PUT">
        </c:if>
        <br>
        Email:<form:input path="email"/>
        <form:errors path="email"></form:errors>
        <br>
        <%
            Map<String ,Integer> genders=new HashMap();
            genders.put("1","Male");
            genders.put("0","Female");
            //不能直接显示，放到域对象里面？？？
            request.setAttribute("genders",genders);
        %>
        <!-- items可以放一个map或者集合 -->
        Gender:<form:radiobutton path="gender" items="${genders}"/>
        <br>
        <!-- itemLabel放置要显示的值-->
        Department:<form:select path="department.id" items="${departments} " itemLabel="departmentName" itemValue="id"></form:select>
        <br>

        <!--
            1.设计类型转化问题(将string转化成date类型)
            2.数据类型格式化
            3.数据校验
                1).如何校验？注解？
                    i.使用JSR 303（Java为bean数据合法性校验提供的标准框架）验证标准
                    ii.加入hibernate validator（是JSR 303的参考实现支持其所有标准的校验注解）验证框架的jar包（加jar包）
                    iii.在Springmvc配置文件中添加<mvc:annotation-driven></mvc:annotation-driven>（一般都已经添加了）
                    iiii.需要在bean的属性上添加对应的注解 eg:
                    iiiii.在目标方法前添加@Valid注解
                2）.验证出错转向哪一个页面？
                3).错误消息？如何显示，如何把错误消息国际化
                    i.显示：直接在form里面加个标签   ： <form:errors path="email"></form:errors>
                    ii.消息国际化:创建配置文件；在配置文件中配置国际化资源文件

        如何解决上述三个问题？？？（数据绑定）
        数据绑定流程：
        见图
        Birth<form:input path="birth"/>
        Salary<form:input path="salary"></form:input> -->
        <input type="submit" value="Submit"/>

    </form:form>
</body>
</html>
