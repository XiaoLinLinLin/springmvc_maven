<%--
  Created by IntelliJ IDEA.
  User: liuyuanlinlin
  Date: 2018/1/25
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <!--
        Springmvc处理静态资源（这是因为使用了js）
        1.为什么会有这个问题？
         REST 资源URL 不希望带 .html 或 .do 等后缀
	    又因为DispatcherServlet 请求映射配置为 /,
	    则 Spring MVC 将捕获 WEB 容器的所有请求, 包括静态资源（图片，js,css）的请求, SpringMVC 会将他们当成一个普通请求处理,
	    因找不到对应处理器将导致错误。
        2.解决方式：
        在springmvc的配置文件中添加 <mvc:default-servlet-handler />
//将get请求转化为post请求？？？
    <script type="text/javascript" src="s"></script>
    <script type="text/javascript">
        $(function () {
            $(".delete").click(function () {
                var href=$(this).attr("href");
                $("form").attr("action",href).submit();
            })
        })
    </script>-->
</head>
<body>
<!-- 注意：这使用的是绝对路径，将post请求转化为delete请求
    <form action="${pageContext.request.contextPath}/emp" method="post">
        <input type="hidden" name="_method" value="DELETE"/>
    </form>-->
<c:if test="${empty requestScope.employees}">
    没有任何员工信息.
</c:if>
<c:if test="${! empty requestScope.employees}">
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>ID</th>
            <th>LastName</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Department</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <c:forEach items="${requestScope.employees}" var="emp">
            <tr>
                <td>${emp.id}</td>
                <td>${emp.lastName}</td>
                <td>${emp.email}</td>
                <td>${emp.gender==0?'Female':'Male'}</td>
                <td>${emp.department.departmentName}</td>
                <td><a href="emp/${emp.id}">Edit</a></td>
                <!-- 这是get请求啊！！！如果转化为delete请求？？？借助于js!!!，
                1、导包
                学完springmvc视频在涉及-->
                <td><a class="delete" href="emp/${emp.id}">Delete</a></td>

            </tr>
        </c:forEach>
    </table>
</c:if>
<br><br>
<a href="/emp"> Add new employee </a>
</body>
</html>
