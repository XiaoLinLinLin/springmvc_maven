package com.spring.crud.handler;


import com.spring.crud.dao.DepartmentDao;
import com.spring.crud.dao.EmployeeDao;
import com.spring.crud.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class EmployeeHandler {
    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private DepartmentDao departmentDao;

    /**
     * 修改操作
     * 1、显示修改页面（回显表单）
     * 2、修改页面，最后重定向到list.jsp
     * @param id
     * @param map
     */
    //解决修改问题（lastname不能修改）
    @ModelAttribute
    public void getEmployee(@RequestParam(value = "id",required = false) Integer id, Map<String ,Object> map){
        if(id!=null){
            map.put("employee",employeeDao.get(id));
        }
    }

    @RequestMapping(value = "/emp",method = RequestMethod.PUT)
    public  String update(Employee employee){
        //这个方法里包含修改
        employeeDao.save(employee);

        return "redirect:/emps";
    }
    //实现回显操作
    @RequestMapping(value = "/emp/{id}",method = RequestMethod.GET)
    public String input(@PathVariable("id") Integer id,Map<String,Object> map){
        map.put("employee",employeeDao.get(id));
        map.put("departments",departmentDao.getDepartments());
        return "input";
    }

    /**
     * 删除操作
     * 难点在jsp页面上
     * @param id
     * @return
     */
    @RequestMapping(value = "/emp/{id}",method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Integer id){
        employeeDao.delete(id);
        return "redirect:/emps";
    }

    /**
     * 添加操作：注意
     * 1、添加页面为get请求
     * 2、完成添加重定向为post请求
     * @param
     * @return
     */


    //这个不是很懂
    @RequestMapping(value = "/emp",method = RequestMethod.POST)
    public String save(@valid Errors result,Map<String,Object> map, Employee employee){
       System.out.println("save:"+employee);
       //如果进行类型转化失败的话，执行如下
       if(result.getErrorCount()>0){
            System.out.println("出错了！");
            for(FieldError error:result.getFieldErrors()){
                System.out.println(error.getField()+":"+error.getDefaultMessage());
            }

            //若验证出错，则转向定制的页面
            map.put("departments",departmentDao.getDepartments());
            return "input"
        }
        //保存操作
        employeeDao.save(employee);



        //通过重定向返回list页面
        return  "redirect:/emps";
    }


    @RequestMapping(value = "/emp",method = RequestMethod.GET)
    public String input(Map<String ,Object>map){
        map.put("departments",departmentDao.getDepartments());
        //对应默认回显，，，但还是不太明白？？？
        map.put("employee",new Employee());
        return "input";
    }




    @RequestMapping("/emps")
    public String list(Map<String ,Object> map){
        map.put("employees",employeeDao.getAll());
        return "list";
    }


    /**
     * 注解@InitBinder的使用
     * @param binder

    @InitBinder
    public void initBinder(WebDataBinder binder){

    }*/
}
