package com.spring.crud.dao;

import com.spring.crud.entities.Department;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DepartmentDao {
    private static Map<Integer,Department> departments=null;
    static{
        departments=new HashMap<Integer, Department>();
        departments.put(101,new Department(101,"D_AA"));
        departments.put(102,new Department(102,"D_BB"));
        departments.put(103,new Department(103,"D_CC"));
        departments.put(104,new Department(104,"D_DD"));
        departments.put(105,new Department(105,"D_EE"));
    }
    public Collection<Department> getDepartments(){
        return departments.values();   //这是什么鬼？？？
    }

    public Department getDepartment(Integer id){
        return departments.get(id);
    }
}
