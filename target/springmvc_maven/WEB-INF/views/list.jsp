<%--
  Created by IntelliJ IDEA.
  User: liuyuanlinlin
  Date: 2018/1/25
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <!--
        Springmvc处理静态资源
        1.
        2.

    <script type="text/javascript" src="s"></script>
    <script type="text/javascript">
        $(function () {
            $(".delete").click(function () {
                var href=$(this).attr("href");
                $("form").attr("action",href).submit();
            })
        })
    </script>-->
</head>
<body>
<!-- 注意：这使用的是绝对路径
    <form action="${pageContext.request.contextPath}/emp" method="post">
        <input type="hidden" name="_method" value="DELETE"/>
    </form>-->
<c:if test="${empty requestScope.employees}">
    没有任何员工信息.
</c:if>
<c:if test="${! empty requestScope.employees}">
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>ID</th>
            <th>LastName</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Department</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <c:forEach items="${requestScope.employees}" var="emp">
            <tr>
                <td>${emp.id}</td>
                <td>${emp.lastName}</td>
                <td>${emp.email}</td>
                <td>${emp.gender==0?'Female':'Male'}</td>
                <td>${emp.department.departmentName}</td>
                <td><a href="emp/${emp.id}">Edit</a></td>
                <!-- 这是get请求啊！！！如果转化为delete请求？？？借助于js!!!-->
                <td><a class="delete" href="emp/${emp.id}">Delete</a></td>
                <a href="/emp"> Add </a>
            </tr>
        </c:forEach>
    </table>
</c:if>
<br><br>

</body>
</html>
